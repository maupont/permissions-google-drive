﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Permissions_Google_Drive.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ResourceMessages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResourceMessages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Permissions_Google_Drive.Resources.ResourceMessages", typeof(ResourceMessages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Creating service....
        /// </summary>
        public static string CreatingService {
            get {
                return ResourceManager.GetString("CreatingService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred: .
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fetching file list....
        /// </summary>
        public static string FetchingFile {
            get {
                return ResourceManager.GetString("FetchingFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fetching permission list....
        /// </summary>
        public static string FetchingPermission {
            get {
                return ResourceManager.GetString("FetchingPermission", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File list fetched..
        /// </summary>
        public static string FileListFetched {
            get {
                return ResourceManager.GetString("FileListFetched", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of files: .
        /// </summary>
        public static string NumberFiles {
            get {
                return ResourceManager.GetString("NumberFiles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission list fetched..
        /// </summary>
        public static string PermissionFetched {
            get {
                return ResourceManager.GetString("PermissionFetched", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permissions successfully removed..
        /// </summary>
        public static string PermissionRemoved {
            get {
                return ResourceManager.GetString("PermissionRemoved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Removing permissions....
        /// </summary>
        public static string RemovingPermission {
            get {
                return ResourceManager.GetString("RemovingPermission", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service successfully created..
        /// </summary>
        public static string ServiceCreated {
            get {
                return ResourceManager.GetString("ServiceCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to String list saved on file: .
        /// </summary>
        public static string TxtSaved {
            get {
                return ResourceManager.GetString("TxtSaved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving string list....
        /// </summary>
        public static string TxtSaving {
            get {
                return ResourceManager.GetString("TxtSaving", resourceCulture);
            }
        }
    }
}
