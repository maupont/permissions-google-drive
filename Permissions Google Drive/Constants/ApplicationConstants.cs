﻿namespace Permissions_Google_Drive.Constants
{
    public class ApplicationConstants
    {
        public static string SettingsSection = "AppSettings"; // Name of the section in the appsettings.json
        public static string Domain = "MainDomain"; // Domain to maintain permissions on the Drive files
        public static string Remove = "RemoveDomain"; // Domain to remove permissions on the Drive files
        public static string ConfigurationFile = "appsettings.json"; // Name of the configuration file
        public static string CredentialsFile = "GoogleAccountServiceCredentialsFile"; // Name of the account service file
        public static string ApplicationName = "DriveApplicationName"; // Name of the Drive API
        public static string ApplicationKey = "DriveAPIKey"; // Name of the Drive API key

        public static string txtExtension = ".txt";

        public static string dateFormat = "yyyyMMdd";
    }
}
