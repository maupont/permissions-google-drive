﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Microsoft.Extensions.Configuration;
using Permissions_Google_Drive.Constants;
using System;
using System.Collections.Generic;
using System.IO;

namespace Permissions_Google_Drive
{
    public class DriveAPIUtils
    {
        public IConfigurationRoot configuration;

        /// <summary>
        /// Instantiate a new DriveAPIUtils. Takes no arguments.
        /// </summary>
        public DriveAPIUtils()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(ApplicationConstants.ConfigurationFile, optional: true, reloadOnChange: true);
            configuration = builder.Build();
        }

        /// <summary>
        /// Create a Drive Service with permissions and configurations from the appsettings.json
        /// </summary>
        /// <returns>A DriveService.</returns>
        public DriveService createDriveService()
        {
            Console.WriteLine(Resources.ResourceMessages.CreatingService);

            // Files must be shared with the account service to be seen
            string[] scopes = new string[] { DriveService.Scope.Drive, DriveService.Scope.DriveAppdata, DriveService.Scope.DriveFile, DriveService.Scope.DriveMetadata };

            GoogleCredential credential;
            using (var stream = new FileStream(configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.CredentialsFile], FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(scopes);
            }

            DriveService service = new DriveService(new BaseClientService.Initializer
            {
                ApplicationName = configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.ApplicationName],
                ApiKey = configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.ApplicationKey],
                HttpClientInitializer = credential
            });
            Console.WriteLine(Resources.ResourceMessages.ServiceCreated);

            return service;
        }

        /// <summary>
        /// Saves a list of e-mails to txt.
        /// </summary>
        /// <param name="listOfFiles">List of files to get permission e-mails.</param>
        public void saveListToTxt(List<string> listString, string fileNamePrefix)
        {
            Console.WriteLine(Resources.ResourceMessages.TxtSaving);

            string fileName = fileNamePrefix + DateTime.Now.ToString(ApplicationConstants.dateFormat) + ApplicationConstants.txtExtension;

            using (TextWriter tw = new StreamWriter(fileName))
            {
                foreach (string s in listString)
                    tw.WriteLine(s);
            }

            Console.WriteLine(Resources.ResourceMessages.TxtSaved + fileName);
        }
    }
}