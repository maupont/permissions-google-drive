﻿using Google;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;
using Microsoft.Extensions.Configuration;
using Permissions_Google_Drive.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DriveFile = Google.Apis.Drive.v2.Data.File;

namespace Permissions_Google_Drive
{
    public class DriveAPI
    {
        private DriveService service;
        private IConfigurationRoot configuration;

        /// <summary>
        /// Instantiate a new DriveAPI.
        /// </summary>
        /// <param name="driveService">Instance of a DriveService.</param>
        public DriveAPI(DriveService driveService)
        {
            service = driveService;
            configuration = new DriveAPIUtils().configuration;
        }

        /// <summary>
        /// Retrieve a list of File resources.
        /// </summary>
        /// <returns>List of File resources.</returns>
        public List<DriveFile> retrieveAllFiles()
        {
            Console.WriteLine(Resources.ResourceMessages.FetchingFile);

            List<DriveFile> result = new List<DriveFile>();
            FilesResource.ListRequest request = service.Files.List();

            do
            {
                try
                {
                    FileList files = request.Execute();
                    result.AddRange(files.Items);
                    request.PageToken = files.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine(Resources.ResourceMessages.Error + e.Message);
                    request.PageToken = null;
                }
            } while (!string.IsNullOrEmpty(request.PageToken));

            Console.WriteLine(Resources.ResourceMessages.FileListFetched);

            return result;
        }

        /// <summary>
        /// Retrieve a list of Permissions.
        /// </summary>
        /// <param name="listOfFiles">List of files to fetch permissions.</param>
        public void retrieveAllPermissions(ref List<DriveFile> listOfFiles)
        {
            Console.WriteLine(Resources.ResourceMessages.FetchingPermission);

            Console.WriteLine(Resources.ResourceMessages.NumberFiles + listOfFiles.Count());
            int counter = 1;

            foreach (DriveFile file in listOfFiles)
            {
                Console.Write("\r{0} of {1}", counter++, listOfFiles.Count());
                PermissionsResource.ListRequest listRequest = service.Permissions.List(file.Id);

                try
                {
                    PermissionList permissions = listRequest.Execute();
                    file.Permissions = permissions.Items;
                }
                catch (Exception e)
                {
                    Console.WriteLine(Resources.ResourceMessages.Error + e.Message);
                    listRequest.PageToken = null;
                }
            }

            Console.WriteLine("");

            Console.WriteLine(Resources.ResourceMessages.PermissionFetched);
        }

        /// <summary>
        /// Removes a permission.
        /// </summary>
        /// <param name="fileId">ID of the file to remove the permission for.</param>
        /// <param name="permissionId">ID of the permission to remove.</param>
        public string RemovePermission(DriveFile file)
        {
            string fileId = file.Id;
            string permissionId = file.Permissions.Where(p => p.Domain == configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.Remove]).
                                FirstOrDefault().Id;
            try
            {
                service.Permissions.Delete(fileId, permissionId).Execute();
            }
            catch (GoogleApiException ex) when (ex.HttpStatusCode == HttpStatusCode.Forbidden)
            {
                return file.AlternateLink;
            }
            catch (GoogleApiException ex) when (ex.HttpStatusCode == HttpStatusCode.NotFound)
            {

            }
            catch (Exception e)
            {
                Console.WriteLine(Resources.ResourceMessages.Error + e.Message);
            }

            return null;
        }

        /// <summary>
        /// Removes multiple permissions with e-mail corresponding to appsettings.json
        /// </summary>
        /// <param name="listOfFilesToUpdate">List of files with permissions to be removed.</param>
        public List<string> RemoveBatchPermission(List<DriveFile> listOfFilesToUpdate)
        {
            Console.WriteLine(Resources.ResourceMessages.RemovingPermission);

            List<string> linksForbiddenFiles = new List<string>();

            foreach (DriveFile file in listOfFilesToUpdate)
            {
                linksForbiddenFiles.Add(RemovePermission(file));
            }

            Console.WriteLine(Resources.ResourceMessages.PermissionRemoved);

            return linksForbiddenFiles;
        }
    }
}
