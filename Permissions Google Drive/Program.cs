﻿using Permissions_Google_Drive.Constants;
using System.Collections.Generic;
using System.Linq;
using DriveFile = Google.Apis.Drive.v2.Data.File;

namespace Permissions_Google_Drive
{
    class Program
    {
        static void Main()
        {
            DriveAPIUtils driveAPIUtils = new DriveAPIUtils();

            DriveAPI driveAPI = new DriveAPI(driveAPIUtils.createDriveService());

            List<DriveFile> listOfFiles = driveAPI.retrieveAllFiles();

            driveAPI.retrieveAllPermissions(ref listOfFiles);

            List<string> emails = listOfFiles.Where(l => l.Permissions != null)
                .SelectMany(f => f.Permissions)
                .Where(d => d.Domain != driveAPIUtils.configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.Domain])
                .Select(s => s.EmailAddress)
                .Distinct()
                .ToList();
            driveAPIUtils.saveListToTxt(emails, nameof(emails));

            List<DriveFile> listOfFilesToUpdate = listOfFiles.
                Where(l => l.Permissions.Any(p => p.Domain == driveAPIUtils.configuration.GetSection(ApplicationConstants.SettingsSection)[ApplicationConstants.Remove])).
                ToList();

            List<string> linksForbiddenFiles = driveAPI.RemoveBatchPermission(listOfFilesToUpdate);

            driveAPIUtils.saveListToTxt(linksForbiddenFiles, nameof(linksForbiddenFiles));
        }
    }
}